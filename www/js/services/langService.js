angular
    .module('mainApp')
    .factory('LangService', ['localStorageService', function (localStorageService) {
        var wordList = {
            en: {
                main: 'Main',
                clearAll: 'Clear All',
                areYuoSure: 'Are you shure?',
                close: 'Close',
                ok: 'Ok',
                addNewList: 'Add new list',
                add: 'Add',
                editList: 'Edit list',
                printCheck: 'Print check',
                hideCheck: 'Hide check',
                totalSum: 'Total sum: ',
                name: 'Name',
                price: 'Price',
                count: 'Count',
                settings: 'Settings',
                language: 'Language',
                symbol: '$'
            },
            ru: {
                main: 'Главная страница',
                clearAll: 'Удалить Все',
                areYuoSure: 'Вы уверенны?',
                close: 'Отмена',
                ok: 'Да',
                addNewList: 'Добавить новый список',
                add: 'Добавить',
                editList: 'Список',
                printCheck: 'Показать чек',
                hideCheck: 'Спрятать чек',
                totalSum: 'Общая сумма: ',
                name: 'Название',
                price: 'Цена',
                count: 'Количество',
                settings: 'Настройки',
                language: 'Язык',
                symbol: '₽'
            },
            ua: {
                main: 'Головна сторінка',
                clearAll: 'Видалити Усе',
                areYuoSure: 'Ви упевненні?',
                close: 'Відміна',
                ok: 'Так',
                addNewList: 'Додати новй список',
                add: 'Додати',
                editList: 'Список',
                printCheck: 'Показати чек',
                hideCheck: 'Сховати чек',
                totalSum: 'Загальна сума: ',
                name: 'Назва',
                price: 'Ціна',
                count: 'Кількість',
                settings: 'Налаштування',
                language: 'Мова',
                symbol: '₴'
            }
        };

        function getLang() {
            return localStorageService.get('lang') || 'en';
        }

        function setLang(val) {
            return localStorageService.set('lang', val);
        }

        function getWords() {
            return wordList[getLang()];
        }

        return {
            getLang: getLang,
            setLang: setLang,
            getWords: getWords
        };
    }]);