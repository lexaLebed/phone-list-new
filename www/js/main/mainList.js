function MainDetailController(EditService, LangService) {
    var editService = EditService;
    var storData;

    this.text = LangService.getWords();
    this.allItems;


    this.render = function(){
        this.allItems = [];
        storData = editService.getItem('mobile');

        for (var prop in storData) {

            var data = {
                name: new Date(Number(prop.substr(1))),
                complete: true
            };

            for(var i = 0; i < storData[prop].length; i++) {
                if (!storData[prop][i].complete) {
                    data.complete = false;
                }
            }

            this.allItems.push(data);
        }
    };

    this.render();

    this.hero = {
        name: 'a'
    };

    this.setEdit = function(index) {
        editService.setEditDetails('d' + (+new Date(this.allItems[index].name)));
    };

    this.clearAll = function() {
        this.allItems = [];
        editService.setItem({});
        editService.setEditDetails('');
    };

    this.removeItem = function(index) {
        delete storData['d' + (+new Date(this.allItems[index].name))];
        editService.setItem(storData);
        this.render();
    };
}

angular
    .module('mainList', [])
    .component('mainOptions', {
        templateUrl: 'js/main/itemlist.html'
    });
