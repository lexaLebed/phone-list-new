angular
    .module('mainApp', [
        'ui.router',
        'LocalStorageModule',
        'mainList',
        'editList',
        'shopList',
        'settings'
    ])
    .config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('main', {
            url: '/',
            templateUrl: 'js/main/itemList.html',
            component: 'mainOptions',
            controller: MainDetailController,
            controllerAs: 'vm'
        })

        .state('edit', {
            url: '/edit',
            templateUrl: 'js/edit/edit.html',
            component: 'editOptions',
            controller: EditController,
            controllerAs: 'vm'
        })

        .state('settings', {
            url: '/settings',
            templateUrl: 'js/settings/settings.html',
            component: 'settingsOptions',
            controller: SettingsController,
            controllerAs: 'vm'
        })

        .state('shop', {
            url: '/shop',
            templateUrl: 'js/shop/shop.html',
            component: 'shopOptions',
            controller: ShopDetailController,
            controllerAs: 'vm'
        });
    });
