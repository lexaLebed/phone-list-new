function ShopDetailController(EditService, LangService) {
    var editService = EditService;
    this.text = LangService.getWords();

    this.currentDate = 'd' + +new Date();
    this.goods = {
        price: null,
        count: 1,
        name: '',
        complete: 0
    };
    this.items = editService.getItem('mobile');
    this.items[this.currentDate] = [];

    this.editData = function() {
        if (this.goods.name) {
            this.items[this.currentDate].unshift(this.goods);
            editService.setItem(this.items);
            this.goods.name = '';
            this.items = editService.getItem('mobile');
        }
    };

    this.editItem = function(index) {
        this.items[this.currentDate][index].price = !this.items[this.currentDate][index].price;

        if (!this.items[this.currentDate][index].price) {
            editService.setItem(this.items);
        }
    };

    this.removeItem = function(index) {
        this.items[this.currentDate].splice(index,1);
        editService.setItem(this.items);
    };
};

angular
    .module('shopList', [])
    .component('shopOptions', {
        templateUrl: 'js/shop/shop.html'
    });
