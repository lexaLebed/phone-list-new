function SettingsController(LangService) {
    this.text = LangService.getWords();

    this.changeLang = function(lang) {
        LangService.setLang(lang);
    }
}

angular
    .module('settings', [])
    .component('settingsOptions', {
        templateUrl: 'js/settings/settings.html',
        controller: SettingsController
    });
