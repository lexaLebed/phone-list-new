function EditController(EditService) {
    this.ok = 'ok';
    var editService = EditService;
    var storData = editService.getItem('mobile');

    this.list = storData[editService.getEditDetails()] || [];

    this.add = {
        name: '',
        price: null,
        count: 1,
        complete: false
    };

    this.editPrice = function(){
        this.total = 0;
        editService.setItem(storData);
        this.countPrice();
    };

    this.countPrice = function() {
        this.total = 0;
        this.list.forEach(function(item, i){
           this.total += item.price * item.count;
        }.bind(this));
    };

    this.editDone = function(item) {
        item.complete = !item.complete;
        this.editPrice();
    };

    this.addItem = function() {

        this.list.push(JSON.parse(JSON.stringify(this.add)));
        this.editPrice();

        this.add = {
            name: '',
            price: null,
            count: 1,
            complete: false
        };
    };

    this.countPrice();
}

angular
    .module('editList', [])
    .component('editOptions', {
        templateUrl: 'js/edit/edit.html',
        controller: EditController
    });
